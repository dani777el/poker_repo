/**
 * Creates a player who has a Hand of Cards as well as tracking their Wins and Turn
 */
package model;

import java.util.Vector;

public class Player 
{

	//Data Members
    private String myName;
    private int myNumberWins;
    private Hand myHand;
    private boolean myIsTurn;
        
	public Player(String name) 
	{
	    myName = name;
	}

	/**
	 * Gets the players name
	 * @return String
	 */
	public String getName() 
	{
		return myName;
	}

	/**
	 * Gets the number of times this player has won.
	 * @return Integer
	 */
	public int getNumberWins() 
	{
		return myNumberWins;
	}

	/**
	 * Increments number of wins
	 */
	public void incrementNumberWins() 
	{
		myNumberWins++;
	}

	/**
	 * Reset win count
	 */
	public void resetWins() 
	{
		myNumberWins = 0;
	}

	public Object clone() 
	{
		return null;
	}

	public String toString()
	{
		return null;
	}

	public Hand getHand()
	{
		return myHand;
	}

	public void setTurn(boolean isTurn)
	{
		myIsTurn = isTurn;
	}

	public boolean isTurn() 
	{
		return myIsTurn;
	}

	public int callGetHandScore() 
	{
		return 0;
	}

	public void callDiscard() 
	{
	    
	}
}