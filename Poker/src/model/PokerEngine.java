package model;

import java.util.Vector;

/**
 * Background calculations for the poker game to allow for creation, calculation, and
 * termination for the game.
 * @author Class' Project
 *
 */
public class PokerEngine 
{
	/**
	 * Data Members to allow for upper and lower bounds of players, player storage, and player
	 * count.
	 */
    private Deck myDeck;
    public final static int MAX_NUM_PLAYERS = 7;
    public final static int MIN_NUM_PLAYERS = 7;
    private Player[] myPlayers;
    private int myNumPlayers;
    /**
     * Constructor for PokerEngine Class:
     * 
     * 
     */
	public PokerEngine() 
	{
	    
	}
	/**
	 * Initiates game play and initializes number of players
	 * @param numberPlayers
	 */
	public void createGame(int numberPlayers) 
	{
	    
	}
	/**
	 * Method to track and increment the turn process of poker
	 */
	public void changeTurn() 
	{
	}
	/**
	 * Method to deal each player's hand for the current game
	 */
	public void dealCards() 
	{
	}
	/**
	 * Calculates hands of each player and determines the winner
	 * @return Player: Who is winner
	 */
	public Player calculateWinner() 
	{
		return null;
	}
	/**
	 * Method to re-initialize the poker game
	 */
	public void resetGame() 
	{
	}
	/**
	 * Method to re-initialize all members and processes to game
	 */
	public void resetAll() 
	{
	}
	/**
	 * toString method
	 * @return 
	 */
	public String toString() 
	{
		return null;
	}
	/**
	 * Method to begin game and turn processes
	 */
	public void startGame() 
	{
	    
	}
	/**
	 * Method for retrieving players from the current game
	 * @return myPlayers: Array of Players
	 */
	public Player[] getPlayers()
	{
	    return myPlayers;
	}
}