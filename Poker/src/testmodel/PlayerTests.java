package testmodel;

import model.Deck;
import model.Player;
import static org.junit.Assert.*;

import org.junit.*;

public class PlayerTests
{
    private Player player1;
    private Player player2;
    /**
     * Creates 2 Player objects to be used in test cases.
     */
    @Before
    public void setup()
    {
        player1 = new Player("player1");
        player2 = new Player("player2");
    }

    /**
     * Tests if the Player class is creating unique names, as ordered by the constructor.
     */
    @Test
    public void testIfUniquePlayerNames()
    {
        assertFalse("The Players have the same name...", player1.getName().equals(player2.getName()));
    }
    
    /**
     * Tests if getName() method returns the right name
     * Date: 03/27/2014 
     * @author: JDalmasso
     */
    @Test
    public void testIfCorrectName()   
    {
    	assertTrue("The name received is not correct", player1.getName().equals("player1"));
    }
    
    /**
     * Tests if the Number of wins is a positive number or 0, not a negative number.
     */
    @Test
    public void testIfNumberWinsPositive()
    {
        assertTrue("The Number of Wins should be a positive number", player1.getNumberWins()>=0);
    }
    
    /**
     * Tests if the number of wins is incremented by one win.
     */
    @Test
    public void testIfNumberWinsIncrements()
    {
       int numWins = player1.getNumberWins();
       player1.incrementNumberWins();
       assertTrue("The Number of Wins should have incremented by one win", player1.getNumberWins()>numWins);
    }

    /**
     * Tests if the number of wins is reset after it was incremented to 1.
     */
    @Test
    public void testIfNumberWinsResets()
    {
    	//adding one to their wins so that if they have zero wins you still
    	//check if reset functions
    	//-Dillon
       player1.incrementNumberWins();
       player1.resetWins();
       assertTrue("The Number of Wins should have reset", player1.getNumberWins()==0);
    }
    
     /**
     * Tests if the method clones successfully and is not merely pointing to the same data member.
     * 
     * @author Jacob Hell 
     */
    @Test
    public void testIfClones()
    {
       player2 = (Player) player1.clone();
       assertFalse("The Player objects should be pointing to unique instances of the Player Class", player1==player2);
       assertTrue("The Player objects should have the same name (They are Clones)", 
               player1.getName().equals(player2.getName()));
       
    }
    
    /**
     * Tests to see if the setTurn method works properly.
     * 
     * @author Jacob Hell
     */
    @Test
    public void seeIfCorrectTurn()
    {
    	player1.setTurn(true);
    	assertFalse("It is the wrong players turn.", player2.isTurn() == true);
    }
    /*
    /** this shouldn't be here because there is no game to determine whose turn it is.
     * Should have a check to prevent multiple people from having turns at the same time
     * 
     * @author Brandon Edwards
    @Test
    public void seeIfMultipleTurns()
    {
    	player1.setTurn(true);
    	player2.setTurn(true);
    	assertFalse("Two Players have the same turn.", player2.isTurn() == true && player1.isTurn() == true);
    }
    */
    
    @Test
    public void testsIfCallsRateHandMethod()    
    {
//        Deck myDeck = new Deck();
        int original = -1;
        int temp = -1;
//        player1.receiveCard(myDeck.draw());
//        player1.receiveCard(myDeck.draw());
//        player1.receiveCard(myDeck.draw());
//        player1.receiveCard(myDeck.draw());
//        player1.receiveCard(myDeck.draw());
        temp = player1.callGetHandScore();
        assertFalse("The rateHand Method was not called.", temp == original);
        assertTrue("The callGetHandScore method returned an Invalid Number (negative).", temp > original);
    }
    
    /**
     * Tests to see if the methods that manipulate wins function well together
     */
    @Test
    public void testWinManipulation()
    {
       // int numWins = player1.getNumberWins();
       player1.incrementNumberWins();
       player1.resetWins();
       player1.incrementNumberWins();
       player1.incrementNumberWins();
       assertTrue("The Number of Wins should be 2 after a reset and two increments", player1.getNumberWins()==2);
    }
}


