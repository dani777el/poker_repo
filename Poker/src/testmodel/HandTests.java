package testmodel;

import static org.junit.Assert.*;
import java.util.Vector;
import org.junit.Test;
import model.Card;
import model.CardSuit;
import model.CardType;
import model.Hand;

public class HandTests
{
    @Test
    public void testHandScore()
    {
        Hand hand = createHandAceHigh();
        
        assertTrue("Hand score should be 14.", hand.getHandScore() == 14);
    }

    @Test
    public void testCompareEqualHands()
    {
        Hand hand1 = createHand4Aces();
        Hand hand2 = createHand4Aces();
        
        assertTrue("Comparing 2 identical hands, should be true.", hand1.compareTo(hand2) == 0);
        assertTrue("Comparing hand to itself, should be true.", hand1.compareTo(hand1) == 0);
    }
    @Test
    public void testCompareUnequalHands()
    {
        Hand hand1 = createHand4Aces();
        Hand hand2 = createHandAceHigh();
        
        assertTrue("Comparing better hand to worse hand, should be true.", hand1.compareTo(hand2) > 0);
        assertTrue("Comparing worse hand to better hand, should be true.", hand2.compareTo(hand1) < 0);
    }
    @Test
    public void testCompareMultipleHands()
    {
        Hand hand1 = createHand4Aces();
        Hand hand2 = createHandAceHigh();
        Hand hand3 = createHandRoyalFlush();
        Hand hand4 = createHandAceHigh2();
        
        assertTrue("Comparing better hand to worse hand, should be true.", hand1.compareTo(hand2) > 0);
        assertTrue("Comparing worse hand to better hand, should be true.", hand2.compareTo(hand1) < 0);
        assertTrue("Comparing better hand to worse hand, should be true.", hand3.compareTo(hand1) > 0);
        assertTrue("Comparing equal hands.", hand2.compareTo(hand4) == 0);
    }
    
    @Test
    public void testDiscard()
    {
    	Hand hand = createHandAceHigh();
    	Vector<Integer> discards = new Vector<Integer>();
    	discards.add(0);
    	hand.discard(discards);
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.KING)));
    	
    	assertTrue("hand score should be 13", hand.getHandScore() == 13);
    }
    
    /**
     * Tests if hand recieves a card.
     * 
     * @author George Robbins
     */
    @Test
    public void testReceivedCard()
    {
    	Hand hand = createHandStraight1();
    	int handScore = hand.getHandScore();
    	
    	Vector<Integer> discards = new Vector<Integer>();
    	discards.add(0);
    	hand.discard( discards );
    	
    	hand.receiveCard( new Card(CardSuit.SPADES, CardType.NINE) );
    	assertTrue("hand changed: should not have equal scores", handScore != hand.getHandScore());
    }
    
    /**
     * Tests if receiveCard() returns the right card
     * Date: 03/27/2014 
     * @author: JDalmasso
     */
    @Test
    public void testReceivedCorrectCard() 
    {
    	Hand hand = createHandAceHigh();
    	
    	Vector<Integer> discards = new Vector<Integer>();
    	discards.add(0);
    	hand.discard(discards);	
    	
    	hand.receiveCard(new Card(CardSuit.SPADES, CardType.KING));
    	int handScore = hand.getHandScore();
    	assertTrue("The card received is not the right one", handScore == 13);
    }
    
    /**
     * Tests if hand can have more cards than allowed.
     * 
     * @author George Robbins
     */
    @Test
    public void testExceedHandLimit()
    {
    	Hand hand = createHandAceHigh();
    	int handScore = hand.getHandScore();
    	
    	hand.receiveCard( new Card(CardSuit.SPADES, CardType.NINE) );
    	assertTrue("Should not add a 6th card: scores should be equal", handScore == hand.getHandScore() );
    }
    
    /**
     * Tests if getHandScore() returns the right score
     * Date: 03/27/2014 
     * @author: JDalmasso
     */
    @Test
    public void testScore() 
    {
    	Hand hand = createHand4Aces();
    	int handScore = hand.getHandScore();
    	
    	assertTrue("The hand was not scored correctly!", handScore == 71414);
    }
    
    @Test
    public void testRoyalFlushVsStraight() 
    {
        Hand hand1, hand2;
        hand1 = createHandRoyalFlush();
        hand2 = createHandRoyalFlush();
        Vector <Integer> discards = new Vector <Integer>(1);
        discards.add(new Integer(0));
        hand2.discard(discards);
        hand2.receiveCard(new Card(CardSuit.HEARTS, CardType.NINE));
        assertTrue("The Royal Flush should have won." ,  hand1.compareTo(hand2) > 0);
    }    

    @Test
    public void testIfAceTo5StraightWorks() 
    {
        Hand hand1 = new Hand();
        hand1.receiveCard(new Card(CardSuit.HEARTS, CardType.FIVE));
        hand1.receiveCard(new Card(CardSuit.CLUBS, CardType.FOUR));
        hand1.receiveCard(new Card(CardSuit.DIAMONDS, CardType.TWO));
        hand1.receiveCard(new Card(CardSuit.SPADES, CardType.THREE));
        hand1.receiveCard(new Card(CardSuit.HEARTS, CardType.ACE));
        
        //4 * 10000(straight) + 5 * 100 (highest card making up groups is 5) + 5 (5 is high card overall)
        assertTrue("This should be rated as a straight with a high card of Five "
                + "and the ace serving as a 1.", hand1.getHandScore() == 40505);
    }    
    
    @Test
    public void testIfAceTo5StraightFlushWorks() 
    {
        Hand hand1 = new Hand();
        hand1.receiveCard(new Card(CardSuit.HEARTS, CardType.FIVE));
        hand1.receiveCard(new Card(CardSuit.HEARTS, CardType.FOUR));
        hand1.receiveCard(new Card(CardSuit.HEARTS, CardType.TWO));
        hand1.receiveCard(new Card(CardSuit.HEARTS, CardType.THREE));
        hand1.receiveCard(new Card(CardSuit.HEARTS, CardType.ACE));
        
        //8 * 10000(straight) + 5 * 100 (highest card making up groups is 5) + 5 (5 is high card overall)
        assertTrue("This should be rated as a straight flush with a high card of Five "
                + "and the ace serving as a 1.", hand1.getHandScore() == 80505);
    }        
    
    @Test
    public void testIfRoyalFlushWorks() 
    {
        Hand hand1 = createHandRoyalFlush();
        //8 * 10000(Straight Flush) + 14 * 100 (highest card making up groups is Ace) + 14 (Ace is high card overall)
        assertTrue("This should be rated as a high straightflush.", hand1.getHandScore() == 81414);        
    }    
    
    @Test
    public void testIfPairVsPair() 
    {
        Hand hand1 = createHandPair2(),
        		hand2 = createHandPair7();
        
        assertTrue("Pair of 7s > 2s.", hand1.compareTo(hand2) < 0 );        
    }  
    
    @Test
    public void testIfPairVsTwoPair() 
    {
        Hand hand1 = createHandPair2(),
        		hand2 = createHandTwoPair1();
        
        assertTrue("Pair < 2 pair.", hand1.compareTo(hand2) < 0 );        
    }  
    
    @Test
    public void testIfPairVsThreeOfKind() 
    {
        Hand hand1 = createHandPair2(),
        		hand2 = createHandThree7();
        
        assertTrue("Pair < three of kind.", hand1.compareTo(hand2) < 0 );        
    }  
    
    @Test
    public void testIfPairVsFourOfKind() 
    {
        Hand hand1 = createHandPair2(),
        		hand2 = createHand4fives();
        
        assertTrue("Pair < Four of kind.", hand1.compareTo(hand2) < 0 );        
    }  
    
    @Test
    public void testIfPairVsStraight() 
    {
        Hand hand1 = createHandPair2(),
        		hand2 = createHandStraight1();
        
        assertTrue("Pair < straight.", hand1.compareTo(hand2) < 0 );        
    }  
 
    @Test
    public void testIfPairVsFlush() 
    {
        Hand hand1 = createHandPair2(),
        		hand2 = createHandFlush1();
        
        assertTrue("Pair < flush.", hand1.compareTo(hand2) < 0 );        
    }  
    
    @Test
    public void testIfPairVsFullHouse() 
    {
        Hand hand1 = createHandPair2(),
        		hand2 = createHandFullHouse1();
        
        assertTrue("Pair < flush.", hand1.compareTo(hand2) < 0 );        
    }  
 
    @Test
    public void testIfPairVsStraightFlush() 
    {
        Hand hand1 = createHandPair2(),
        		hand2 = createHandStraightFlush1();
        
        assertTrue("Pair < straight flush.", hand1.compareTo(hand2) < 0 );        
    }  

    @Test
    public void testIfPairVsRoyalFlush() 
    {
        Hand hand1 = createHandPair2(),
        		hand2 = createHandRoyalFlush();
        
        assertTrue("Pair < royal flush.", hand1.compareTo(hand2) < 0 );        
    }  

    @Test
    public void testIfTwoPairVsTwoPair() 
    {
        Hand hand1 = createHandTwoPair2(),
        		hand2 = createHandTwoPair1();
        

        assertTrue("two pair 6 & 2 < two pair 7 & 5.", hand1.compareTo(hand2) < 0 );    
    }  
    
    @Test
    public void testIfTwoPairVsThreeOfKind() 
    {
        Hand hand1 = createHandTwoPair1(),
        		hand2 = createHandThree7();
        
        assertTrue("Two pair < three of kind.", hand1.compareTo(hand2) < 0 );        
    }  
    
    @Test
    public void testIfTwoPairVsFourOfKind() 
    {
        Hand hand1 = createHandTwoPair1(),
        		hand2 = createHand4fives();
        
        assertTrue("Two pair 2 < four of kind.", hand1.compareTo(hand2) < 0 );        
    }  
    
    @Test
    public void testIfTwoPairVsStraight() 
    {
        Hand hand1 = createHandTwoPair1(),
        		hand2 = createHandStraight1();
        
        assertTrue("Two pair 2 < straight.", hand1.compareTo(hand2) < 0 );        
    }  
    
    @Test
    public void testIfTwoPairVsFlush() 
    {
        Hand hand1 = createHandTwoPair1(),
        		hand2 = createHandFlush1();
        
        assertTrue("Two pair 2 < flush.", hand1.compareTo(hand2) < 0 );        
    }  
    
    @Test
    public void testIfTwoPairVsStraightFlush() 
    {
        Hand hand1 = createHandTwoPair1(),
        		hand2 = createHandStraightFlush1();
        
        assertTrue("Two pair 2 < straight flush.", hand1.compareTo(hand2) < 0 );        
    }  
    
    @Test
    public void testIfTwoPairVsFullHouse() 
    {
        Hand hand1 = createHandTwoPair1(),
        		hand2 = createHandFullHouse1();
        
        assertTrue("Two pair 2 < full house.", hand1.compareTo(hand2) < 0 );        
    }  
    
    @Test
    public void testIfTwoPairVsRoyalFlush() 
    {
        Hand hand1 = createHandTwoPair1(),
        		hand2 = createHandRoyalFlush();
        
        assertTrue("Two pair 2 < royal flush.", hand1.compareTo(hand2) < 0 );        
    } 
    
    @Test
    public void testIfThreeOfKindVsThreeOfKind() 
    {
        Hand hand1 = createHandThree7(),
        		hand2 = createHandThree9();
        
        assertTrue("three 7s < three 9s.", hand1.compareTo(hand2) < 0 );        
    }  
    
    @Test
    public void testIfThreeOfKindVsStraight() 
    {
        Hand hand1 = createHandThree7(),
        		hand2 = createHandStraight1();
        
        assertTrue("three 7s < Straight.", hand1.compareTo(hand2) < 0 );        
    }  

    @Test
    public void testIfThreeOfKindVsFlush() 
    {
        Hand hand1 = createHandThree7(),
        		hand2 = createHandFlush1();
        
        assertTrue("three 7s < flush.", hand1.compareTo(hand2) < 0 );        
    }  

    @Test
    public void testIfThreeOfKindVsStraightFlush() 
    {
        Hand hand1 = createHandThree7(),
        		hand2 = createHandStraightFlush1();
        
        assertTrue("three 7s < straight flush.", hand1.compareTo(hand2) < 0 );        
    }  

    @Test
    public void testIfThreeOfKindVsFullHouse() 
    {
        Hand hand1 = createHandThree7(),
        		hand2 = createHandFullHouse1();
        
        assertTrue("three 7s < full house.", hand1.compareTo(hand2) < 0 );        
    }  
    
    @Test
    public void testIfThreeOfKindVsRoyalFlush() 
    {
        Hand hand1 = createHandThree7(),
        		hand2 = createHandRoyalFlush();
        
        assertTrue("three 7s < royal flush.", hand1.compareTo(hand2) < 0 );        
    }  

    @Test
    public void testIfFourOfKindVsFourOfKind() 
    {
        Hand hand1 = createHand4fives(),
        		hand2 = createHand4sevens();
        
        assertTrue("four 5s < four 7s.", hand1.compareTo(hand2) < 0 );        
    }  

    @Test
    public void testIfFourOfKindVsStraight() 
    {
        Hand hand1 = createHandStraight1(),
        		hand2 = createHand4fives();
        
        assertTrue("straight < four 5s.", hand1.compareTo(hand2) < 0 );        
    }  

    @Test
    public void testIfFourOfKindVsFlush() 
    {
        Hand hand1 = createHandFlush1(),
        		hand2 = createHand4fives();
        
        assertTrue("flush < four 5s.", hand1.compareTo(hand2) < 0 );        
    }  

    @Test
    public void testIfFourOfKindVsStraightFlush() 
    {
        Hand hand1 = createHand4fives(),
        		hand2 = createHandStraightFlush1();
        
        assertTrue("four 5s < straight flush.", hand1.compareTo(hand2) < 0 );        
    }  

    @Test
    public void testIfFourOfKindVsRoyalFlush() 
    {
        Hand hand1 = createHand4fives(),
        		hand2 = createHandRoyalFlush();
        
        assertTrue("four 5s < royal flush.", hand1.compareTo(hand2) < 0 );        
    }  
    
    @Test
    public void testIfStraightVsStraight() 
    {
        Hand hand1 = createHandStraight1(),
        		hand2 = createHandStraight2();
        
        assertTrue("staight with six high < straight with seven high.", hand1.compareTo(hand2) < 0 );        
    }  

    @Test
    public void testIfStraightVsFlush() 
    {
        Hand hand1 = createHandStraight1(),
        		hand2 = createHandFlush1();
        
        assertTrue("staight with six high < flush.", hand1.compareTo(hand2) < 0 );        
    }  

    @Test
    public void testIfStraightVsFullHouse() 
    {
        Hand hand1 = createHandStraight1(),
        		hand2 = createHandFullHouse1();
        
        assertTrue("staight with six high < full house.", hand1.compareTo(hand2) < 0 );        
    }  

    @Test
    public void testIfStraightVsStraightFlush() 
    {
        Hand hand1 = createHandStraight1(),
        		hand2 = createHandStraightFlush1();
        
        assertTrue("staight with six high < straight flush.", hand1.compareTo(hand2) < 0 );        
    }  
    
    @Test
    public void testIfStraightVsRoyalFlush() 
    {
        Hand hand1 = createHandStraight1(),
        		hand2 = createHandRoyalFlush();
        
        assertTrue("staight with six high < royal flush.", hand1.compareTo(hand2) < 0 );        
    }  

    @Test
    public void testIfFlushVsFullHouse() 
    {
        Hand hand1 = createHandFlush1(),
        		hand2 = createHandFullHouse1();
        
        assertTrue("flush < full house.", hand1.compareTo(hand2) < 0 );        
    }  

    @Test
    public void testIfFlushVsStraightFlush() 
    {
        Hand hand1 = createHandFlush1(),
        		hand2 = createHandStraightFlush1();
        
        assertTrue("flush < straight flush.", hand1.compareTo(hand2) < 0 );        
    }  

    @Test
    public void testIfFlushVsRoyalFlush() 
    {
        Hand hand1 = createHandFlush1(),
        		hand2 = createHandRoyalFlush();
        
        assertTrue("flush < royal flush.", hand1.compareTo(hand2) < 0 );        
    }  

    @Test
    public void testIfFullHouseVsFullHouse() 
    {
        Hand hand1 = createHandFullHouse2(),
        		hand2 = createHandFullHouse1();
        
        assertTrue("full house eights high < full house nines high.", hand1.compareTo(hand2) < 0 );        
    }  

    @Test
    public void testIfFullHouseVsStraightFlush() 
    {
        Hand hand1 = createHandFullHouse1(),
        		hand2 = createHandStraightFlush1();
        
        assertTrue("full house eights high < straight flush.", hand1.compareTo(hand2) < 0 );        
    }  
    
    @Test
    public void testIfFullHouseVsRoyalFlush() 
    {
        Hand hand1 = createHandFullHouse1(),
        		hand2 = createHandRoyalFlush();
        
        assertTrue("full house eights high < royal flush.", hand1.compareTo(hand2) < 0 );        
    }  

    @Test
    public void testIfStraightFlushVsStraightFlush() 
    {
        Hand hand1 = createHandStraightFlush1(),
        		hand2 = createHandStraightFlush2();
        
        assertTrue("straight flush with 6 < straight flush with 7 high.", hand1.compareTo(hand2) < 0 );        
    }  
    
    @Test
    public void testIfStraightFlushVsRoyalFlush() 
    {
        Hand hand1 = createHandStraightFlush1(),
        		hand2 = createHandRoyalFlush();
        
        assertTrue("straight flush with 6 < toyal flush.", hand1.compareTo(hand2) < 0 );        
    }  

	//helper function to create a hand of 4 aces and an 8 of clubs, score should be 71414
    public Hand createHand4Aces()
    {
        Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.ACE)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.ACE)));
        hand.receiveCard((new Card(CardSuit.SPADES, CardType.ACE)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.ACE)));
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.EIGHT)));
        return hand;
    }
    
  //helper function to create a hand of 4 fives and an 8 of clubs
    public Hand createHand4fives()
    {
        Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.FIVE)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.FIVE)));
        hand.receiveCard((new Card(CardSuit.SPADES, CardType.FIVE)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.FIVE)));
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.TWO)));
        return hand;
    }
    
    public Hand createHand4sevens()
    {
        Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.SEVEN)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.SEVEN)));
        hand.receiveCard((new Card(CardSuit.SPADES, CardType.SEVEN)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.SEVEN)));
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.TWO)));
        return hand;
    }

    //helper function to create a hand with an ace highcard, score should be 14
    public Hand createHandAceHigh()
    {
        Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.ACE)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.FIVE)));
        hand.receiveCard((new Card(CardSuit.SPADES, CardType.JACK)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.SIX)));
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.EIGHT)));
        return hand;
    }
    //helper function to create top hand
    public Hand createHandRoyalFlush()
    {
        Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.TEN)));
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.JACK)));
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.QUEEN)));
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.KING)));
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.ACE)));
        return hand;
    }
  //helper function to create a hand with an ace highcard, score should be the same as ACEHIGH
    public Hand createHandAceHigh2()
    {
        Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.ACE)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.FIVE)));
        hand.receiveCard((new Card(CardSuit.SPADES, CardType.JACK)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.SIX)));
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.EIGHT)));
        return hand;
   }
  //helper function to create a hand with an queen highcard, score should be 12 
    public Hand createHandQueenHigh()
    {
        Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.QUEEN)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.FIVE)));
        hand.receiveCard((new Card(CardSuit.SPADES, CardType.JACK)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.SIX)));
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.EIGHT)));
        return hand;
    }
  //helper function to create a hand with a flush with an ace high
    public Hand createHandFlush1()
    {
        Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.ACE)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.FIVE)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.JACK)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.SIX)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.EIGHT)));
        return hand;
    }
  //helper function to create a hand with a flush with an queen high
    public Hand createHandFlush2()
    {
        Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.QUEEN)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.FIVE)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.JACK)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.SIX)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.EIGHT)));
        return hand;
    }
  //helper function to create a hand with a pair of 2s
    public Hand createHandPair2()
    {
        Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.QUEEN)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.FIVE)));
        hand.receiveCard((new Card(CardSuit.SPADES, CardType.TWO)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.SIX)));
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.TWO)));
        return hand;
    }
  //helper function to create a hand with a pair of 7s
    public Hand createHandPair7()
    {
        Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.SEVEN)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.FIVE)));
        hand.receiveCard((new Card(CardSuit.SPADES, CardType.JACK)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.TWO)));
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.SEVEN)));
        return hand;
    }
    //helper function to create a hand with a pair of 7s and a pair of 5s
    public Hand createHandTwoPair1()
    {
        Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.SEVEN)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.FIVE)));
        hand.receiveCard((new Card(CardSuit.SPADES, CardType.JACK)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.FIVE)));
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.SEVEN)));
        return hand;
    }
    //helper function to create a hand with a pair of 6s and a pair of 2s
    public Hand createHandTwoPair2()
    {
        Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.SIX)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.TWO)));
        hand.receiveCard((new Card(CardSuit.SPADES, CardType.JACK)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.TWO)));
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.SIX)));
        return hand;
    }
  //helper function to create a hand with three 7s
    public Hand createHandThree7()
    {
        Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.SEVEN)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.SEVEN)));
        hand.receiveCard((new Card(CardSuit.SPADES, CardType.JACK)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.TWO)));
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.SEVEN)));
        return hand;
    }
  //helper function to create a hand with three 9s
    public Hand createHandThree9()
    {
        Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.NINE)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.FIVE)));
        hand.receiveCard((new Card(CardSuit.SPADES, CardType.NINE)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.TWO)));
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.NINE)));
        return hand;
    }
    //helper function to create a hand with three 9s and two 2s
    public Hand createHandFullHouse1()
    {
        Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.NINE)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.FIVE)));
        hand.receiveCard((new Card(CardSuit.SPADES, CardType.NINE)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.FIVE)));
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.NINE)));
        return hand;
    }
  //helper function to create a hand with three 8s and two 2s
    public Hand createHandFullHouse2()
    {
        Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.EIGHT)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.TWO)));
        hand.receiveCard((new Card(CardSuit.SPADES, CardType.EIGHT)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.TWO)));
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.EIGHT)));
        return hand;
    }
    
    public Hand createHandStraight1() 
    {
 		Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.TWO)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.THREE)));
        hand.receiveCard((new Card(CardSuit.SPADES, CardType.FOUR)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.FIVE)));
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.SIX)));
        return hand;
 	}
    
    public Hand createHandStraight2()
    {
 		Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.SEVEN)));
        hand.receiveCard((new Card(CardSuit.HEARTS, CardType.THREE)));
        hand.receiveCard((new Card(CardSuit.SPADES, CardType.FOUR)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.FIVE)));
        hand.receiveCard((new Card(CardSuit.CLUBS, CardType.SIX)));
        return hand;
 	}
    
    public Hand createHandStraightFlush1()
    {
 		Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.TWO)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.THREE)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.FOUR)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.FIVE)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.SIX)));
        return hand;
 	}
 
    public Hand createHandStraightFlush2()
    {
 		Hand hand = new Hand();
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.SEVEN)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.THREE)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.FOUR)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.FIVE)));
        hand.receiveCard((new Card(CardSuit.DIAMONDS, CardType.SIX)));
        return hand;
 	}

}
