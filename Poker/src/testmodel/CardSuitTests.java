/**
 * Test Enumeration Card Suit
 */
package testmodel;

import static org.junit.Assert.*;

import org.junit.Test;

import model.CardSuit;

public class CardSuitTests
{

	/**
	 * Test to make sure suit is set correctly
	 */
    @Test
    public void testGetCardSuit()
    {
        CardSuit cardSuit;
        cardSuit = CardSuit.SPADES;
        assertFalse("Suit is wrong", cardSuit.getSuit() == CardSuit.HEARTS.getSuit());   
    }
    
	/**
	 * Test to make sure suit is set correctly
	 */
    @Test
    public void testGetCardCorrectSuit()
    {
        CardSuit cardSuit;
        cardSuit = CardSuit.SPADES;
        assertTrue("Suit is wrong", cardSuit.getSuit() == CardSuit.SPADES.getSuit());   
    }

}