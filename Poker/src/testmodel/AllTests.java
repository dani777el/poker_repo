package testmodel;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CardSuitTests.class, CardTests.class, CardTypeTests.class, DeckTests.class, HandTests.class, PlayerTests.class, PokerEngineTests.class })
public class AllTests {

}
